# SICOM 3A and Master SIGMA program: Statistical/Machine learning course

## <font color="red">`News`</font>

##### Homework before the second course on **Monday, September 14**

- read and run the [introduction notebooks](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/notebooks/1_introduction/) `N1_Linear_Classification.ipynb` and `N2_Polynomial_Classification_Model_Complexity.ipynb`
- answer the questions of the notebook exercises and upload it (pdf file from your editor,  or scanned pdf file of a handwritten sheet) under chamilo in the [assignment tool](https://chamilo.grenoble-inp.fr/main/work/work_list.php?cidReq=PHELMA5PMSAST6&id_session=0&gidReq=0&gradebook=0&origin=&id=117272) (those and only those who do not yet have an agalan account can send it to me by email):
  - only text explanations are required, no need to copy/paste figure or graphics!
  - must not exceed half a length of A4 paper

##### ~~first course session will take place Monday afternoon, September 7 at Minatec (face-to-face).~~

## Welcome to the Statistical Learning course!

You will find in this gitlab repository the necessary material for the teaching of _Machine Learning_:

- course materials for the lessons ([slides](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/slides))
- examples and exercises for the labs in the form of [Jupyter python notebooks](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/notebooks) (`.ipynb` files) and/or via online applications,
- quiz (links to the online tool [Socrative](https://b.socrative.com/login/student/))

These resources will be updated as the sessions progress.

### How to use the notebooks?

The examples and exercises will be done under python 3.x through [scikit-learn](https://scikit-learn.org/), and also [tensorflow](https://www.tensorflow.org/). These are two of the most widely used machine learning packages.

The _Jupyter Notebooks_ (`.ipynb` files) are programs containing both cells of code (for us Python) and cells of markdown text for the narrative side. These notebooks are often used to explore and analyze data. Their processing is done with a `jupyter-notebook`, or `juypyter-lab` application, which is accessed through a web browser.

In order to run them you have several possibilities:

1. Download the notebooks to run them on your machine. This requires a Python environment (> 3.3), and the Jupyter notebook and scikit-learn packages. It is recommended to install them via the [anaconda](https://www.anaconda.com/downloads) distribution which will directly install all the necessary dependencies.

**Or**

1. Use the _mybinder_ service ans links to run them interactively and remotely (online): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fchatelaf%2Fml-sicom3a/master?urlpath=lab/tree/notebooks) (open the link and wait a few seconds for the environment to load).<br>
  **Warning:** Binder is meant for _ephemeral_ interactive coding, meaning that your own modifications/codes/results will be lost when your user session will automatically shut down (basically after 10 minutes of inactivity)

**Or**

1. Use a `jupyterhub` online service:

  - we recommend the UGA's service, [jupyterhub.u-ga.fr](https://jupyterhub.u-ga.fr), so that you can run your notebooks on the UGA's computation server while saving your modifications and results. Also useful to launch a background computation (connection with your Agalan account; requires uploading your notebooks+data to the server).
  - alternatively you can use an equivalent `jupyterhub` service. For example the one from google, namely [google-colab](https://colab.research.google.com/), which allows you to run/save your notebooks and also to _share the edition to several collaborators_ (requires a google account and upload your notebooks+data in your Drive)

**Note :** You will also find among the notebooks an introduction to Python [notebooks/0_python_in_a_nutshell](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/tree/master/notebooks%2F0_python_in_a_nutshell)

### Miscellaneous remarks on the materials

- The slides are designed to be self-sufficient (even if the narrative side is often limited by the format).
- In addition to the slides and bibliographical/web references, we generally propose links or videos (at the beginning or end of the slides) specific to the concepts presented. These lists are of course not exhaustive, and you will find throughout the web many resources, often pedagogical. Feel free to do your own research. <!-- and share it on the [Riot room](https://riot.ensimag.fr/#/room/#sicom-ml:ensimag.fr) if you find it useful. -->

<!-- - [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fchatelaf%2Fml-sicom3a/54301940e4486a8ece22a910c3efa1b2734ed82d?filepath=notebooks) link to run the examples, *except Deep learning ones* too computationally demanding for the JupyterHub server (use the first solution to run these notebooks with your own ressources...) -->
